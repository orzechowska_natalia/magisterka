# based on : https://medium.com/@habibdhif/simple-makefile-to-automate-python-projects-e233af7681ad and
# https://github.com/artofai/overcome-the-chaos/blob/master/Makefile
# https://blog.horejsek.com/makefile-with-python/


SHELL=/bin/bash

VENV_NAME?=venv
VENV_BIN=$(shell pwd)/${VENV_NAME}/Scripts
VENV_ACTIVATE=. ${VENV_BIN}/activate
PYTHON=${VENV_BIN}/python

venv:
        ( \
                source  $(VENV_NAME)/Scripts/activate; \
                pip3 install -r requirements.txt; \
        )
project_name = magisterka

clean:
        rm -f $(shell pwd)/dev-0/*.tsv
        rm -f $(shell pwd)/dev-1/*.tsv
