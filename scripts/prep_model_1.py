import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score
import datetime as dt

myindex = 1
features = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/in_cechy.tsv',sep='\t')
features['encoded_word'] = [ int.from_bytes(word.encode("utf-8"),byteorder='big') for word in features.loc[:,'word'].values ]
first_date_train_final= features.iloc[0]['year']
last_date_train = features.iloc[-1]['year']+1
daterange = pd.date_range(str(first_date_train_final),str(last_date_train),freq='Y').year
for year in daterange:
    features.loc[features['year'] == year , 'year'] = myindex
    myindex = myindex + 1
first_cols = ['word','encoded_word','year']
features =  features.drop(['duration_start','zakodowane_słowo'],axis = 1)
features_cols = features.columns.tolist()
new_order = first_cols+[ col for col in features_cols if col not in first_cols]
features = features[new_order]
Y = features.iloc[:,3].values
#words = features[features['word']]
exclude = ['word','occurences']
X = features.drop(exclude,axis = 1)
X = X.values
print('bye')

X_train, X_test, Y_train, Y_test = train_test_split( X, Y, test_size=0.2, random_state=0)
regressor = LinearRegression()
regressor.fit(X_train,Y_train)
y_pred= regressor.predict(X_test)
print(mean_squared_error(Y_test,y_pred))