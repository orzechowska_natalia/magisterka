import hashlib
from scripts.generate_interim import InterimDataGenerator
from scripts.generate_in import InGenerator

# podział słów na dwa foldery w zależności od zakodowania md5

class Md5(object):

    def share_random(self,path_with_files_to_share):

        interimdatagenerator = InterimDataGenerator()
#        ingenerator = InGenerator()

        file_list = interimdatagenerator.load_file_list(path_with_files_to_share)

        dev_0 = []
        dev_1 = []

        for word in file_list:
            old_word = word
            word = word.replace('.tsv','')
            md5hex = hashlib.md5(word.encode('UTF-8')).hexdigest()
            md5bin = bin(int(md5hex,16))[2:].zfill(8)
            if str(md5bin)[-1:] == '0':
                dev_0.append(old_word)
            else:
                dev_1.append(old_word)

        return dev_0,dev_1

