#from scripts.prepare_model_3 import get_data,get_target
import random
from scripts.to_supervised_prep import difference
from keras.layers import RepeatVector, Dense, TimeDistributed , GRU , LSTM , Input , Bidirectional , Dropout
from sklearn.metrics import mean_squared_error , mean_absolute_error
from keras.models import Model , Sequential
from keras.optimizers import Adam,Nadam,RMSprop
from keras.layers import Dropout
from scripts.attention_decoder import AttentionDecoder
import numpy as np
import random
from scripts.to_supervised_prep import difference
import pandas as pd
from keras.models import model_from_json
from scripts.generate_interim import  InterimDataGenerator

def get_data_train():
    list_of_dfs = []
    interimdatagenerator = InterimDataGenerator()
    base_dir = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia'
    list_of_files = interimdatagenerator.load_file_list(base_dir)
    for file in list_of_files:
        dir = base_dir + '/' + file
        dataframe = pd.read_csv(dir,sep='\t',names=['date','occurences'])
        word = file.replace('.tsv','')
        dataframe['word'] = word
        dataframe = dataframe.fillna(0)
        dataframe['date'] = pd.to_datetime(dataframe['date'], format='%Y-%m',errors='coerce').dt.strftime('%Y-%m')
        dataframe['date'] = pd.to_datetime(dataframe['date'], format='%Y-%m')
        dataframe_series = dataframe.groupby(dataframe['date'].dt.year).sum()
        dataframe_fin = pd.DataFrame({'word':word, 'date': dataframe_series.index.values,
                                    'occurences': dataframe_series.values.flatten()})
        mean_to_replace = dataframe_fin['occurences'].mean()
        try:
            last_date_train = dataframe_fin.iloc[0]['date']
            first_date_train = dataframe_fin.iloc[-1]['date']
            daterange = pd.date_range(start=str(last_date_train), end = str(first_date_train), freq='Y').year
            df_final = pd.DataFrame({'word': word, 'date': daterange.values, 'occurences': 0})
            for date in daterange:
                occurences = dataframe_fin.loc[dataframe_fin['date'] == date]
                if occurences.shape[0] != 0:
                    ilosc = occurences.iloc[0]['occurences']
                    df_final.loc[df_final['date'] == date, 'occurences'] = ilosc
                else:
                    df_final.loc[df_final['date'] == date, 'occurences'] = mean_to_replace
        except:
            continue
        else:
            df_final['x'] = df_final['occurences'].shift(1)
            df_final = df_final.fillna(0)
            list_of_dfs.append(df_final)
            print(word)
    list_of_dfs = pd.concat(list_of_dfs)
    return list_of_dfs


def get_data_test():
    in_test = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep='\t',names = ['word','date'])
    expected_0 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/expected.tsv',sep='\t',names = ['occurences'])
    in_test['occurences'] = expected_0.loc[:,'occurences']
    in_test['x'] = in_test['occurences'].shift(1)

    in_test_2 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/in.tsv',sep='\t',names = ['word','date'])
    expected_1 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/expected.tsv', sep='\t', names=['occurences'])
    in_test_2['occurences'] = expected_1.loc[:, 'occurences']
    in_test_2['x'] = in_test_2['occurences'].shift(1)

    in_test.append(in_test_2)
    return in_test

def sampling(values,length):
    n = len(values)
    samples = []
    # step over the 5,000 in jumps of 200
    for i in range(0, n, length):
        sample_tr = values[i:i + length]
        if len(sample_tr) == length:
            samples.append(sample_tr)
    return samples



#train,test = get_data()
#final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)


#final_train['word'] =  train_rest['word']
#final_train['year'] = train_rest['year']
#final_train['x'] = train_target.iloc[:,0]
#final_train = final_train.fillna(0)

#final_test['word'] = test_rest['word']
#final_test['year'] = test_rest['year']


#final_test['x'] = test_target.iloc[:,0]
#final_test = final_test.fillna(0)


#ords = final_train['encoded_word'].to_list()
list_of_dfs = get_data_train()
test = get_data_test()
words_no_dupli = list(set(list_of_dfs['word'].to_list()))
word_dict = {}

for word in words_no_dupli:
    word_pandas = list_of_dfs.loc[list_of_dfs['word'] == word]
    word_pandas_ts = test.loc[test['word'] == word]

    train_target = word_pandas[['occurences']]
    test_target = word_pandas_ts[['occurences']]

    word_df_tr = word_pandas.sort_values(by='date')
    word_df_ts = word_pandas_ts.sort_values(by='date')

    word_df_tr = word_df_tr[['x']]
    word_df_ts = word_df_ts[['x']]

    word_df_tr = np.array(word_df_tr)
    word_df_ts = np.array(word_df_ts)
    word_df_tr = difference(word_df_tr,1)
    word_df_ts = difference(word_df_ts,1)

    #word_df_tr -= word_df_tr.min()
    #word_df_tr /= word_df_tr.max()

    #word_df_ts -= word_df_ts.min()
    #word_df_ts /= word_df_ts.max()

    train_target = np.array(train_target)
    test_target = np.array(test_target)
    train_target = difference(train_target,1)
    test_target = difference(test_target,1)

    #test_target -= test_target.min()
    #test_target /= test_target.max()

    #train_target -= train_target.min()
    #train_target /= train_target.max()

    #allseries = word_df_tr.append(word_df_ts)
    #allseries = allseries['x'].shift(1)
    #allseries = allseries.fillna(0)
    #ser = difference(allseries.values,1)
    #word_occurences_train, word_occurences_test = ser[0:-word_df_ts.shape[0]], ser[-word_df_ts.shape[0]:]

    word_occurences_test= word_df_ts.values.tolist()
    word_occurences_train = word_df_tr.values.tolist()

    train_target = train_target.values.tolist()
    test_target = test_target.values.tolist()

    samples_train = sampling(word_occurences_train, 4)
    samples_test = sampling(word_occurences_test, 4)
    samples_train_target = sampling(train_target, 4)
    samples_test_target = sampling(test_target, 4)

    samples_train = np.array(samples_train)
    samples_test = np.array(samples_test)
    samples_train_target = np.array(samples_train_target)
    samples_test_target = np.array(samples_test_target)

    try:
        samples_train = samples_train.reshape(samples_train.shape[0], samples_train.shape[1], 1)
        samples_test = samples_test.reshape(samples_test.shape[0], samples_test.shape[1], 1)

        samples_train_target = samples_train_target.reshape(samples_train_target.shape[0], samples_train_target.shape[1], 1)
        samples_test_target = samples_test_target.reshape(samples_test_target.shape[0], samples_test_target.shape[1], 1)
    except:
        pass
    else:
        word_dict[word] = [samples_train, samples_train_target, samples_test, samples_test_target]



example_sample_train = list(word_dict.values())[0][0]
example_sample_test =  list(word_dict.values())[3][0]
len_units = len(list(word_dict.items()))
model = Sequential()
model.add(LSTM(300, activation = 'relu', input_shape=(example_sample_train.shape[1],example_sample_train.shape[2])
              , dropout= 0.5))
model.add(RepeatVector(example_sample_train.shape[1]))
model.add(LSTM(300, activation='linear', return_sequences=True, dropout= 0.5))
model.add(TimeDistributed(Dense(example_sample_test.shape[1], activation='linear')))
model.add(Dropout(0.5))
model.add(TimeDistributed(Dense(1)))
model.add(Dropout(0.5))
optimizer = Nadam( lr = 0.0001)
model.compile(loss='mse', optimizer=optimizer )

model.summary()

print('bye')
print('bye')

for word in word_dict.items():
    try:
        to_fit = word_dict[word[0]]
        model.fit(to_fit[0],to_fit[1], epochs=100, validation_split=0.4 )
    except:
        pass


for word in word_dict.items():
    to_pred =  word_dict[word[0]]
    #samp_test = samples_test[2].reshape(1, samples_test.shape[1], samples_test.shape[2])
    #samp_test_target = samples_test_target[i].reshape(1, samples_test_target.shape[1],
                                                            #samples_test_target.shape[2])
    #to_pred = np.reshape(to_pred,(1,to_pred.shape[0],to_pred.shape[1]))
    predicted = model.predict(to_pred[2])
    before_pred = word_dict[word[0]]
    before_pred.append(predicted)
    word_dict[word[0]] = before_pred


list_of_dfs = []
dictionary = {}
index = 0
for word in word_dict.items():
    new_dict = word_dict[word[0]]
    new_dict_val = new_dict[-2:]
    new_dict_val[0] = new_dict_val[0].flatten()
    new_dict_val[1] =  new_dict_val[1].flatten()
    dictionary['real'] = new_dict_val[0].tolist()
    dictionary['predicted'] = new_dict_val[1].tolist()
    dataframe = pd.DataFrame(dictionary)
    dataframe['word'] = word[0]
    test_rest_word = test.loc[test['word'] == word[0]]
    year_list = test_rest_word['date'].tolist()
#    for index,item in enumerate(year_list):
#        if index != 5:
#            row_name = str(year_list[index]) + '-'+ str(year_list[index+1])
#            new_year_list.append(row_name)
    dataframe['year'] = year_list[:-1]
    list_of_dfs.append(dataframe)

all = pd.concat(list_of_dfs)
all = all.fillna(0)

words =  all['word'].to_list()
words_no_dupli = list(set(words))

mse = mean_squared_error(all['real'],all['predicted'])
mae = mean_absolute_error(all['real'],all['predicted'])
print(mse)
print(mae)
for i,word in enumerate(words_no_dupli):
    word_pandas = all.loc[ all['word'] == word ]
    title = word + 'LSTM Seq2Seq Model3'
    word_pandas_plot = word_pandas.set_index('year').plot(title =  title).get_figure()
    filename = title + '.png'
    filepath = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/plots/'+filename
    word_pandas_plot.savefig(filepath)

#overalmse

model.summary()
# serialize model to JSON
model_json = model.to_json()
with open("LSTM_Seq2Seq_Model3.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model_GRU2X100_dropout2X0.5.hdf5")

print('bye')
