from scripts.prepare_model_3 import get_target,get_data
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, RepeatVector, Dense, TimeDistributed , GRU
from tensorflow.keras.activations import relu
import numpy as np
import random

##batch_size = 10
#epochs = 10

# train LSTM
train,test = get_data()
final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)
#for sample in range(5000):
# generate new random sequence
#X,y = get_pair(n_timesteps_in, n_timesteps_out, n_features)
#X_data.append(X)
#y_data.append(y)

#X_data = array(X_data).reshape(5000, X.shape[1], X.shape[2])
#y_data = array(y_data).reshape(5000, X.shape[1], X.shape[2])

#model_1 = Sequential()
#input
#model_1.add(Dense(256, kernel_initializer='normal',input_dim = final_train.shape[1], activation='linear'))
#hidden
#model_1.add(Atte)

#model.fit(X_data, y_data, batch_size = batch_size, epochs = epochs, verbose = 2)




# these are just made up hyperparameters, change them as you wish
#idden_size = 50

#seq2seq = Sequential([

    #LSTM(hidden_size, input_shape=(final_train.shape[0], 3)),

    #RepeatVector(1)

#    LSTM(hidden_size, return_sequences=True)

    #Dense(hidden_size, activation='linear')

    #TimeDistributed(Dense(1, activation='linear'))

#])
#seq2seq.compile(optimizer='rmsprop',
#              loss='mse')
final_train = final_train.values.tolist()
final_test = final_test.values.tolist()
train_target = train_target.values.tolist()
test_target = test_target.values.tolist()
#final_train = np.array(final_train)
#final_test = np.array(final_test)

# split into samples (e.g. 5000/200 = 25)
samples_train = []
samples_test = []
length = 200
n = len(final_train)
# step over the 5,000 in jumps of 200
for i in range(0,n,length):
    sample_tr = final_train[i:i+length]
    if len(sample_tr) == 200:
        samples_train.append(sample_tr)

l = len(final_test)
for j in range(0,l,length):
    sample_ts = final_test[j:j+length]
    if len(sample_ts) == 200:
        samples_test.append(sample_ts)

k = len(final_train)
samples_train_target = []
for k in range(0,k,length):
    sample_trt = train_target[k:k+length]
    if len(sample_trt) == 200:
        samples_train_target.append(sample_trt)

m = len(final_test)
samples_test_target = []
for m in range(0,m,length):
    sample_tst = train_target[m:m+length]
    if len(sample_tst) == 200:
        samples_test_target.append(sample_tst)

samples_train = np.array(samples_train)
samples_test = np.array(samples_test)
samples_train_target = np.array(samples_train_target)
samples_test_target = np.array(samples_test_target)

#final_train = np.reshape(final_train,(1,final_train.shape[0], final_train.shape[1]))
#final_test = np.reshape(final_test,(1,final_test.shape[0],final_test.shape[1]))
# split into samples (e.g. 5000/200 = 25

#train_target = train_target.values.tolist()
#train_target = np.array(train_target)
#train_target = np.reshape(train_target,(1,train_target.shape[0],train_target.shape[1]))

model = Sequential()
model.add(GRU(200, activation='linear', input_shape=(samples_train.shape[1],samples_train.shape[2])))
model.add(RepeatVector(samples_train_target.shape[1]))
model.add(GRU(200, activation='linear', return_sequences=True))
model.add(TimeDistributed(Dense(50, activation='linear')))
model.add(TimeDistributed(Dense(1)))
model.compile(loss='mse', optimizer='nadam')

model.fit(samples_train, samples_train_target, epochs=1 , validation_split= 0.3 )
predictions = []
for i in range(samples_test.shape[0]):
    to_pred = samples_test[i]
    to_pred = np.reshape(to_pred,(1,to_pred.shape[0],to_pred.shape[1]))
    predicted = model.predict(to_pred,verbose=0)
    predictions.append(predicted)

predictions = np.array(predictions)
real = np.array(test_target)
real = real.flatten()
predictions = predictions.flatten()
real = real[:len(predictions)]
test_rest = test_rest.iloc[:len(predictions)]
test_rest['predicted'] = predictions
test_rest['real'] = real
words = test_rest['word'].to_list()
words_no_dupli = list(set(words))
for i in range(5):
    random_i = random.randint(0,len(words_no_dupli)-1)
    word_random = words_no_dupli[random_i]
    word_pandas = test_rest.loc[ test_rest['word'] == word_random ]
    title = word_random + ' encoder_decoder_GRU2X200'
    word_pandas_plot = word_pandas.set_index('year').plot(title =  title)

print('bye')