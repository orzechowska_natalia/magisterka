
from pandas import read_csv
from matplotlib import pyplot
from pandas.plotting import lag_plot
from scripts.prepare_model_3 import get_data,get_target
from statsmodels.graphics.tsaplots import plot_acf

##batch_size = 10
#epochs = 10

# train LSTM
train,test = get_data()
final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)



series = final_train.iloc[:,1]
lag_plot(series)
pyplot.show()


plot_acf(series, lags=31)
pyplot.show()