import pandas as pd
from numpy.testing import print_coercion_tables

from scripts.generate_interim import InterimDataGenerator
import datetime as dt

interimdatagranerator = InterimDataGenerator()

base_dir = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia'

in_file_1 =  pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/dummies.tsv',sep='\t')
#in_file_1['duration_start'] = in_file_1['duration_start'].dt.date
in_file_1['duration_start'] = pd.to_datetime(in_file_1['duration_start'], dayfirst=True, errors = 'coerce').dt.strftime('%Y-%m')
in_file_list = interimdatagranerator.load_file_list('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia')
in_file_to_compare = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep='\t',names = ['word','year'])
in_file_to_compare_2 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/in.tsv',sep='\t',names = ['word','year'])
list_of_dfs = []

for word in in_file_list:
    file_path = base_dir + '/' + word
    final_word = word.replace('.tsv','')
    pandas_word = pd.read_csv(file_path,sep='\t',names=['date','occurences'])
    pandas_word_dummies = in_file_1.loc[in_file_1['word'] == final_word]
    pandas_word_dummies['duration_start'] =  pd.to_datetime(pandas_word_dummies['duration_start'],
                                                            dayfirst=True, errors = 'coerce').dt.strftime('%Y')
    pandas_word_dummies['year'] = pd.to_datetime(pandas_word_dummies['duration_start'],format='%Y').dt.year
    pandas_in = in_file_to_compare.loc[in_file_to_compare['word'] == final_word]
    if pandas_in.shape[0] == 0:
        pandas_in = in_file_to_compare_2.loc[in_file_to_compare_2['word'] == final_word]
    if pandas_in.shape[0] != 0:
        pandas_word['date'] = pd.to_datetime(pandas_word['date'], dayfirst=True, errors = 'coerce').dt.strftime('%Y-%m')
        last_date_train = pandas_in.iloc[0]['year']
        first_date_train= dt.datetime.strptime(pandas_word.iloc[0]['date'],'%Y-%m')
        first_date_train_final = first_date_train.year
        daterange = pd.date_range(str(first_date_train_final),str(last_date_train),freq='Y').year
        pandas_word['date'] = pd.to_datetime(pandas_word['date'],format='%Y-%m')
        dataframe = pandas_word.groupby(pandas_word['date'].dt.year).size()
        dataframe = pd.DataFrame({'year': dataframe.index, 'ilosc_wystapien': dataframe.values})
        df_occurecnes_and_years = pd.DataFrame({'word':final_word,'year':daterange.values,'occurences':0})
        for date in daterange:
            ilosc_wystapien = dataframe.loc[dataframe['year'] == date]
            if ilosc_wystapien.shape[0]!= 0:
                ilosc = ilosc_wystapien.iloc[0]['ilosc_wystapien']
                df_occurecnes_and_years.loc[df_occurecnes_and_years['year'] == date,'occurences'] = ilosc
        df_occurecnes_and_years['year'] = pd.to_datetime(df_occurecnes_and_years['year'],format='%Y').dt.year
        df_occurecnes_and_years['year'] = df_occurecnes_and_years['year'].values.astype(int)
        pandas_word_dummies['year'] = pandas_word_dummies['year'].values.astype(int)
        final_df = df_occurecnes_and_years.merge(pandas_word_dummies,on=['word','year'],how = 'left')
        final_df = final_df[final_df['origin'].notna()]
        list_of_dfs.append(final_df)
    else:
        print('ups!')
single_df = pd.concat(list_of_dfs)
single_df = single_df.sort_values(by=['year'])
single_df.to_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/in_cechy.tsv',sep='\t',index=False)
print('bye')