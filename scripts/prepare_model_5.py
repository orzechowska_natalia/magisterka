from scripts.prepare_model_3 import get_data, get_target
from keras.models import Sequential
import numpy as np
import random
from keras.layers import Dense, Activation, Flatten
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from xgboost import XGBRegressor


import pandas as pd



train,test = get_data()
final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)


model = XGBRegressor()
model.fit(final_train,train_target,verbose=False)

# Get the mean absolute error on the validation data
predicted = model.predict(final_test)
MSE = mean_squared_error(test_target , predicted)
print('XGBRegressor forest validation MSE = ', MSE)

predicted_df = pd.DataFrame(predicted,columns=['predicted'])
test2_words_array = test_rest.to_numpy()
y_pred = np.append(test2_words_array,predicted_df,axis = 1)
y_pred = np.append(test_target,y_pred,axis=1)
y_pred_df = pd.DataFrame(y_pred,columns=['real','word','year','predicted'])
y_pred_df = y_pred_df.reindex(columns=['word','year','real','predicted'])
words = y_pred_df['word'].to_list()
words_no_dupli = list(set(words))
for i in range(5):
    random_i = random.randint(0,len(words_no_dupli)-1)
    word_random = words_no_dupli[random_i]
    word_pandas = y_pred_df.loc[ y_pred_df['word'] == word_random ]
    title = word_random + '_xgboostregressor'
    word_pandas = word_pandas.set_index('year').plot(title =  title)