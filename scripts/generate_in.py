import pandas as pd

class InGenerator(object):

    def generate_input(self,file_list,input_directory,output_direcotry):

        df_list = []
        slownik = {}

        for file_txt in file_list:
            filename = input_directory + '/' + file_txt
            dataframe = pd.read_csv(filename,sep='\t',names = ['data_wystapienia','ilosc_wystapien'])
            try:
                sample_row = dataframe.sample()
            except:
                continue
            sample_row['data_wystapienia'] = pd.to_datetime(sample_row['data_wystapienia'], errors='coerce')
            sample_row['data_wystapienia'] = sample_row['data_wystapienia'].dt.year
            word = file_txt.replace('.tsv','')
            for i in range(0,6):
                if i == 0:
                    year = sample_row['data_wystapienia'].values[0]
                else:
                    year += 1
                    slownik['slowo'] = word
                    slownik['rok'] = year
                    df_list.append(slownik)
                    slownik = {}


        filename2 = output_direcotry + '/' + 'in.tsv'
        df = pd.DataFrame(df_list)
        df = df.dropna()
        df.to_csv(filename2, sep='\t',index=False,header=None)
        print('bye')

