import json
from builtins import list
import pandas as pd
import numpy as np
import re
import morfeusz2
import os
import datetime

class InterimDataGenerator(object):

    def get_morfeusz(self):

        # morfeusz by uzyskać trzon/temat słowa

        morf = morfeusz2.Morfeusz(expand_tags = True)

        return morf

    def load_file_list(self,directory_name):

        #ładowanie plików z ścieżki

        list_of_files = os.listdir(directory_name)
        return list_of_files

    def create_tsv(self, morf, list_of_files, directory_name, output_path_1, output_path_2):

        # tworzenie .tsv dla wystapien dla danego slowa ( jedno slowo jeden plik )
        # tworzenie opis.tsv dla każdego słowa (jedno słowo jeden plik )

        begin = "1824-01-01"
        end = '2003-12-31'
        begin_datetime = pd.to_datetime(begin,errors='coerce').tz_localize('Europe/Brussels')
        end_datetime = pd.to_datetime(end,errors='coerce').tz_localize('Europe/Brussels')

        for file in list_of_files:

            word = file.replace('.json','')

            # możliwe formy dla danego słowa by usprawnić wyszukiwanie w tekście

            analysis = morf.generate(word)

            words_to_find = [ word[0] for word in analysis]

            directory_name_final = directory_name + '/' + file

            with open(directory_name_final,encoding='UTF-8') as f:
                data = json.load(f)

            train = data['response']['docs']
            highlighting = data['highlighting']

            dataFrame = pd.DataFrame(train)

            dataFrame['occurences'] = 0

            # wyszukiwanie słów w tekście

            for i in range(len(dataFrame)):
                tekst = dataFrame.iloc[i]['content']
                tekst = tekst.lower()
                occurences = len(re.findall(r"(?=("+'|'.join(words_to_find)+r"))",tekst))
                dataFrame.at[i,'occurences'] = occurences
                highlight_content = highlighting[dataFrame.iloc[i]['id']]
                try:
                    dataFrame.at[i,'highlighting'] =  repr(highlight_content['content'][0])
                except:
                #  pusty słownik
                    dataFrame.at[i,'highlighting'] = 'X'
            try:
                dataFrame['duration_start'] = pd.to_datetime(dataFrame['duration_start'],errors='coerce')
                dataFrame['duration_end'] = pd.to_datetime(dataFrame['duration_end'],errors='coerce')
            except:
                continue

            if len(dataFrame) != 0:
                dataFrame = dataFrame.loc[(dataFrame['duration_start'] > begin_datetime) & (dataFrame['duration_start'] < end_datetime)]
                dataFrame =  dataFrame[['lname','iid','duration_start','duration_end','occurences','highlighting']]
                dataFrame.insert(0,'word',word)

                dataFrame2 = dataFrame[['word','duration_start','occurences']]

                dataFrame2 = dataFrame2.groupby(['duration_start']).size()

                filename = word + '.tsv'
                filename2 = word + '_opis' + '.tsv'
                filename = output_path_1 + '/' + filename
                filename2 = output_path_2 + '/' + filename2
                dataFrame2.to_csv(filename, index=True, sep='\t')
                dataFrame.to_csv(filename2, sep='\t')




#morfeusz = get_morfeusz()
#file_list = load_file_list('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/raw')
#create_tsv(morf = morfeusz ,directory_name='C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/raw',list_of_files=file_list)








