from scripts.generate_interim import  InterimDataGenerator
import pandas as pd

interimdatagenerator = InterimDataGenerator()
list_of_dfs = []

base_dir =  'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia'
list_of_files = interimdatagenerator.load_file_list(base_dir)
in_file_to_compare = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep='\t',names = ['word','year'])
in_file_to_compare_2 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/in.tsv',sep='\t',names = ['word','year'])


for file in list_of_files:
    dir = base_dir + '/' + file
    dataframe = pd.read_csv(dir,sep='\t',names=['date','occurences'])
    word = file.replace('.tsv','')
    dataframe['word'] = word
    dataframe['date'] = pd.to_datetime(dataframe['date'], dayfirst=True, errors='coerce').dt.strftime('%Y-%m')
    dataframe['date'] = pd.to_datetime(dataframe['date'], format='%Y-%m')
    dataframe_series = dataframe.groupby(dataframe['date'].dt.year).size()
    pandas_in = in_file_to_compare.loc[in_file_to_compare['word'] == word]
    if pandas_in.shape[0] == 0:
        pandas_in = in_file_to_compare_2.loc[in_file_to_compare_2['word'] == word]
    if pandas_in.shape[0] != 0:
        dataframe_final = pd.DataFrame({'word':word, 'year': dataframe_series.index.values,
                                    'occurences': dataframe_series.values})
        last_date_train = pandas_in.iloc[0]['year']
        first_date_train= dataframe_final.iloc[0]['year']
        daterange = pd.date_range(str(first_date_train),str(last_date_train),freq='Y').year
        df_occurecnes_and_years = pd.DataFrame({'word': word, 'year': daterange.values, 'occurences': 0})
        for date in daterange:
            occurences = dataframe_final.loc[dataframe_final['year'] == date]
            if occurences.shape[0]!= 0:
                ilosc = occurences.iloc[0]['occurences']
                df_occurecnes_and_years.loc[df_occurecnes_and_years['year'] == date,'occurences'] = ilosc
        df_occurecnes_and_years.dropna()
        encoded_word = [int.from_bytes(word.encode("utf-8"), byteorder='big') for word in
                        df_occurecnes_and_years.loc[:, 'word'].values]
        df_occurecnes_and_years['encoded_word'] = encoded_word
        cols = [col for col in df_occurecnes_and_years if col != df_occurecnes_and_years.columns[-1] ]
        cols.insert(1,'encoded_word')
        df_occurecnes_and_years = df_occurecnes_and_years[cols]
        list_of_dfs.append(df_occurecnes_and_years)
    else:
        print('ups!')

one_huge_df = pd.concat(list_of_dfs)
one_huge_df = one_huge_df.sort_values(by=['year'])

one_huge_df['encoded-1'] = one_huge_df['encoded_word'].shift(1)
one_huge_df['occurences-1'] = one_huge_df['occurences'].shift(1)

cols2 = [col for col in one_huge_df if col not in ['encoded-1','year','occurences-1','word']]
cols2.insert(0, 'word')
cols2.insert(1,'year')
cols2.insert(2,'encoded-1')
cols2.insert(3,'occurences-1')
one_huge_df = one_huge_df[cols2]

one_huge_df.to_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/in_supervised.tsv',sep='\t',index=False)
print('bye')


