from scripts.generate_expected import GenerateIn
from scripts.generate_in import InGenerator
from scripts.generate_interim import  InterimDataGenerator
from scripts.generate_expected_random import GenerateExpected
from scripts.md5example import Md5
import pandas as pd

#
# generate_output(input_dir='wyjsciowe_pliki',output_dir='dev-0')
# generate_output(input_dir='wejsciowe_plik',output_dir='dev-1')

ingenerator = InGenerator()
md5 = Md5()
interimdatagenerator = InterimDataGenerator()

# flow programu do tworzenia wyzwania

#tworzenie foldeu interim (opisy słow i ich zliczanie -> każde słowo jeden plik)

#zakomentowane bo byl exception
#morfeusz = interimdatagenerator.get_morfeusz()
#file_list = interimdatagenerator.load_file_list('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/raw')
#interimdatagenerator.create_tsv(morf = morfeusz ,directory_name='C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/raw',
#                                list_of_files=file_list,
#                                output_path_1='C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia',
#                                output_path_2='C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/opisy_slow')

#podział losowy słów według algorytmu md5
dev_0,dev_1, = md5.share_random('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia')

ingenerator.generate_input(dev_0, 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia',
                           'C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0')
ingenerator.generate_input(dev_1, 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia',
                           'C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1')

generate_expected = GenerateExpected()
in_file0 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep = '\t',header=None)
in_file1 = in_file = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/in.tsv',sep = '\t',header=None)
generate_expected.get_expected('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/expected.tsv',in_file0)
generate_expected.get_expected('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-1/expected.tsv',in_file1)