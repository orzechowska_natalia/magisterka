from keras.layers import RepeatVector, Dense, TimeDistributed , GRU , LSTM , Input
from keras.models import Model
from scripts.attention_decoder import AttentionDecoder
import numpy as np
import random
from scripts.prepare_model_3 import get_data,get_target
from scripts.to_supervised_prep import difference
import pandas as pd

##batch_size = 10
#epochs = 10

# train LSTM
train,test = get_data()
final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)


series_tr = train_target.iloc[:,0]
series_ts = test_target.iloc[:,0]
series = series_tr.append(series_ts)

# excluding hashed word to see if it will make it better

# removing autocorrelation
ser = difference(series.values,1)
final_train, final_test = ser[0:-series_ts.shape[0]], ser[-series_ts.shape[0]:]
final_train_df = pd.DataFrame(final_train,columns = ['x'])
final_train_df['x-1'] = final_train.shift(1)
final_train_df = final_train_df.fillna(0)

final_test_df = pd.DataFrame(final_test,columns = ['x'])
final_test_df['x-1'] = final_test.shift(1)
final_test_df = final_test_df.fillna(0)

train_target = final_train_df.iloc[:,0].values.tolist()
test_target = final_test_df.iloc[:,0].values.tolist()

final_train = np.array(final_train_df.iloc[:,-1])
final_test = np.array(final_test_df.iloc[:,-1])

final_train = final_train.tolist()
final_test = final_test.tolist()

#final_train = np.array(final_train)
#final_test = np.array(final_test)

# split into samples (e.g. 5000/200 = 25)
samples_train = []
samples_test = []
length = 20
n = len(final_train)
# step over the 5,000 in jumps of 200
for i in range(0,n,length):
    sample_tr = final_train[i:i+length]
    if len(sample_tr) == 20:
        samples_train.append(sample_tr)

l = len(final_test)
for j in range(0,l,length):
    sample_ts = final_test[j:j+length]
    if len(sample_ts) == 20:
        samples_test.append(sample_ts)

k = len(final_train)
samples_train_target = []
for k in range(0,k,length):
    sample_trt = train_target[k:k+length]
    if len(sample_trt) == 20:
        samples_train_target.append(sample_trt)

m = len(final_test)
samples_test_target = []
for m in range(0,m,length):
    sample_tst = test_target[m:m+length]
    if len(sample_tst) == 20:
        samples_test_target.append(sample_tst)

samples_train = np.array(samples_train)
samples_test = np.array(samples_test)
samples_train_target = np.array(samples_train_target)
samples_test_target = np.array(samples_test_target)


# one feature 179 samples with 200 timesteps
samples_train = samples_train.reshape(samples_train.shape[0],samples_train.shape[1],1)
samples_test = samples_test.reshape(samples_test.shape[0],samples_test.shape[1],1)

samples_train_target = samples_train_target.reshape(samples_train_target.shape[0],samples_train_target.shape[1],1)
samples_test_target = samples_test_target.reshape(samples_test_target.shape[0],samples_test_target.shape[1],1)


i = Input(shape=(samples_train.shape[1],samples_train.shape[2]), dtype='float32')
enc =  LSTM(150, return_sequences=True)(i)
dec  = AttentionDecoder(150,samples_train.shape[2])(enc)
model = Model( inputs = i , outputs = dec )
model.compile(loss='mse', optimizer='adam')


model.summary()
for j in range(samples_train.shape[0]):
    samp_train = samples_train[j].reshape(1, samples_train.shape[1], samples_train.shape[2])
    samp_train_target = samples_train_target[j].reshape(1, samples_train_target.shape[1],
                                                     samples_train_target.shape[2])
    model.fit(samp_train,samp_train_target, epochs=1)


predictions = []
for i in range(samples_test.shape[0]):
    samp_test = samples_test[i].reshape(1, samples_test.shape[1], samples_test.shape[2])
    samp_test_target = samples_test_target[i].reshape(1, samples_test_target.shape[1],
                                                            samples_test_target.shape[2])
    #to_pred = np.reshape(to_pred,(1,to_pred.shape[0],to_pred.shape[1]))
    predicted = model.predict(samp_test)
    predictions.append(predicted)

predictions = np.array(predictions)
predictions = predictions.flatten()
#real = np.array(test_target)
#predictions = predictions[:,0]
real = test_target[:,0]
real = real[:len(predictions)]
test_rest = test_rest.iloc[:len(predictions)]
test_rest['predicted'] = predictions
test_rest['real'] = real
words = test_rest['word'].to_list()
words_no_dupli = list(set(words))
for i in range(5):
    random_i = random.randint(0,len(words_no_dupli)-1)
    word_random = words_no_dupli[random_i]
    word_pandas = test_rest.loc[ test_rest['word'] == word_random ]
    title = word_random + ' encoder_decoder_GRU_1X150_nadam_Attention'
    word_pandas_plot = word_pandas.set_index('year').plot(title =  title)

print('bye')