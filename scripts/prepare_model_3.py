from keras.callbacks import ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import seaborn as sb
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)
import random




def get_data():
    # get train data
    train_data_path = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/in_supervised.tsv'
    train = pd.read_csv(train_data_path,sep='\t')
    encoded_train = train['encoded_word']
    encoded_train_shift = train['encoded-1']
#    encoded_train = [ int(word)/1000000000000 for word in encoded_train]
    encoded_train = [ hash(word) % 10**8 for word in encoded_train]
    train['encoded_word'] = encoded_train
    train['encoded-1'] = train['encoded_word'].shift(1)
    # get test data
    test_data_path = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv'
    expected_data_path = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/expected.tsv'
    test = pd.read_csv(test_data_path,names=['word','year'],sep='\t')
    expected = pd.read_csv(expected_data_path, names=['occurences'], sep='\t')
    encoded_word_test = test['word']
    encoded_word = [ hash(word) % 10**8 for word in encoded_word_test]
    test['encoded_word'] = encoded_word
    cols = [col for col in test if col != test.columns[-1]]
    cols.insert(1, 'encoded_word')
    test = test[cols]
    final_test = pd.concat([test,expected],axis=1)
    final_test = final_test.sort_values(by=['year'])
    final_test['encoded-1'] = final_test['encoded_word'].shift(1)
    final_test['occurences-1'] =  final_test['occurences'].shift(1)
    cols2 = [col for col in test if col not in ['encoded-1', 'year', 'occurences-1', 'word']]
    cols2.insert(0, 'word')
    cols2.insert(1, 'year')
    cols2.insert(2, 'encoded-1')
    cols2.insert(3,'occurences-1')
    cols2.append('occurences')
    final_test = final_test[cols2]
    train.fillna(0)
    final_test.fillna(0)

    return train, final_test


def get_target(test,train):
    mm_scaler_x = MinMaxScaler()
    test_target = test.iloc[:,-1].to_frame()
    test = test.drop(test.columns[-1],axis=1)
    test_rest = test.iloc[:,0:2]
    test = test.drop(test.columns[0],axis=1)
    test = test.drop(test.columns[0],axis=1)
#   test.drop(['occurences'],axis=1,inplace = True)
    train_target = train.iloc[:,-1].to_frame()
    train_rest = train.iloc[:,0:2]
    train = train.drop(train.columns[-1],axis=1)
    train =  train.drop(train.columns[0],axis=1)
    train = train.drop(train.columns[0],axis=1)
    train = train.fillna(0)
    test  = test.fillna(0)
# bez maxmin scaller
    train -=train.min()
    train /=train.max()
    test -=test.min()
    test /=test.max()
    test_target -=test_target.min()
    test_target /=test_target.max()
    train_target -=train_target.min()
    train_target /=train_target.max()

    return test, train, train_target, test_target, train_rest, test_rest

def get_expected_words():
    test_data_path = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv'
    test = pd.read_csv(test_data_path, names=['word', 'year'], sep='\t')
    return test

train,test = get_data()
final_test,final_train,train_target,test_target,train_rest,test_rest = get_target(test,train)

NN_model = Sequential()

# The Input Layer :
NN_model.add(Dense(256, kernel_initializer='normal',input_dim = final_train.shape[1], activation='linear'))

# The Hidden Layers :
NN_model.add(Dense(256, kernel_initializer='normal',activation='linear'))
NN_model.add(Dense(256, kernel_initializer='normal',activation='linear'))
NN_model.add(Dense(256, kernel_initializer='normal',activation='linear'))

# The Output Layer :
NN_model.add(Dense(1, kernel_initializer='normal',activation='linear'))

# Compile the network :
NN_model.compile(optimizer='rmsprop',
              loss='mse')
#NN_model.summary()

#checkpoint_name = 'Weights-{epoch:03d}--{val_loss:.5f}.hdf5'
#checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose = 2, save_best_only = True, mode ='auto')
#callbacks_list = [checkpoint]


#NN_model.fit(final_train, train_target, epochs=300, batch_size=30, validation_split = 0.3, callbacks=callbacks_list)

wights_file = 'Weights-082--44.24299.hdf5' # choose the best checkpoint
NN_model.load_weights(wights_file) # load it

NN_model.compile(loss='mse', optimizer='adam', metrics=['mse'])

y_pred = NN_model.predict(final_test)


test2_words_array = test_rest.to_numpy()
y_pred = np.append(test2_words_array,y_pred,axis = 1)
y_pred = np.append(test_target,y_pred,axis=1)
y_pred_df = pd.DataFrame(y_pred,columns=['real','word','year','predicted'])
y_pred_df = y_pred_df.reindex(columns=['word','year','real','predicted'])
words = y_pred_df['word'].to_list()
words_no_dupli = list(set(words))
##for i in range(5):
#   random_i = random.randint(0,len(words_no_dupli)-1)
#    word_random = words_no_dupli[random_i]
#    word_pandas = y_pred_df.loc[ y_pred_df['word'] == word_random ]
#    title = word_random + 'Weights-082--44.24299.hdf5'
#    word_pandas = word_pandas.set_index('year').plot(title =  title)

print('bye')