import pandas as pd
from scripts.generate_interim import InterimDataGenerator
import datetime as dt

base_dir = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia'
interimgenerator = InterimDataGenerator()
list_of_tsvs = interimgenerator.load_file_list('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia')
in_to_compare_0 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep='\t',names=['word','year'])
in_to_compare_1 = pd.read_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/dev-0/in.tsv',sep='\t',names=['word','year'])

for tsv in list_of_tsvs:
    full_path = base_dir + '/' + tsv
    word_df = pd.read_csv(full_path,sep='\t',names=['date','occurences'])
    word_df['date'] = pd.to_datetime(word_df['date'] , format='%Y-%m-%d %H:%M:%S')
    word_df['date'] = pd.to_datetime(word_df['date'],format='%Y').dt.year
    word_df = word_df.groupby(['date'])['occurences'].count()
    final_word = tsv.replace('.tsv','')
    pandas_in = in_to_compare_0.loc[in_to_compare_0['word'] == final_word]
    if pandas_in.shape[0] == 0:
        pandas_in = in_to_compare_1.loc[in_to_compare_1['word'] == final_word]
    last_date_train = pandas_in.iloc[0]['year']
    word_df = word_df.to_frame()
    word_df['date'] = word_df.index
    word_df = word_df.reset_index()
    first_date_train_final = word_df.iloc[0]['date']
    daterange = pd.date_range(str(first_date_train_final), str(last_date_train), freq='Y').year
    for date in daterange:
        ilosc_wystapien = word_df.loc[word_df['year'] == date]
        if ilosc_wystapien.shape[0] != 0:

    print('bye')