from scripts.generate_interim import InterimDataGenerator
import pandas as pd

#tworzenie plików z oczekiwaną iloscia wystapien

# czytanie pliku in.tsv



dir='C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/słowa_wystąpienia'

# czytanie każdego pliku z katalogu wyjsciowe pliki

interimdatagenerator = InterimDataGenerator()

class GenerateExpected(object):

    #tworzenie pliku expected.tsv dla już wygenerowanego in.tsv

    def __init__(self):
        self.interimdatagenerator = interimdatagenerator

    def list_of_files_to_list_of_dicts(self,dir):
        file_list = interimdatagenerator.load_file_list(dir)
        pandas_dir = {}
        for file in file_list:
            word_name = file.replace('.tsv','')
            directory = dir + '/' + file
            dataFrame = pd.read_csv(directory,sep='\t',header=None,names = ['data','ilosc_wystapien'])
            pandas_dir[word_name] = dataFrame
        return pandas_dir

    mydir = 'dev-1/expected.tsv'

    def get_expected(self,expected_dir,in_file):

        pandas_dir = self.list_of_files_to_list_of_dicts(dir)

        wordslist = []

        #zliczanie ilosci wystapien dla wiersza wylosowanego z in.tsv i wrzucanie go do expected.tsv
        for row in range(in_file.shape[0]):
            word = in_file.iloc[row][0]
            year = pd.to_datetime(in_file.iloc[row][1], format = '%Y').year
            dataframe = pandas_dir[word]
            dataframe['data'] = pd.to_datetime(dataframe['data'], errors='coerce')
            dataframe = dataframe.groupby(dataframe['data'].dt.year).size()
            dataframe = pd.DataFrame({'rok':dataframe.index, 'ilosc_wystapien':dataframe.values})
            ilosc_wystapien = dataframe.loc[ dataframe['rok'] == year ]
            if ilosc_wystapien.shape[0] == 0:
                ilosc_wystapien = 0
            else:
                ilosc_wystapien = ilosc_wystapien.iloc[0]['ilosc_wystapien']
            wordslist.append(ilosc_wystapien)

        expected_df = pd.DataFrame(wordslist)

        expected_df.to_csv(expected_dir,sep='\t',index=False,header=None)













