from pandas import DataFrame
from pandas import Series
from pandas import concat
from pandas import read_csv
from pandas import datetime
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from math import sqrt
import numpy as np
from matplotlib import pyplot
from numpy import array
#from scripts.prepare_model_3 import get_data,get_target
from keras.layers import RepeatVector, Dense, TimeDistributed , GRU , LSTM , Input
from keras.models import Model
from scripts.attention_decoder import AttentionDecoder
#from scripts.prepare_model_3 import get_data,get_target



# date-time parsing function for loading the dataset
def parser(x):
    return datetime.strptime('190' + x, '%Y-%m')


# convert time series into supervised learning problem
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    n_vars = 1 if type(data) is list else data.shape[1]
    df = DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
    # put it all together
    agg = concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


# create a differenced series
def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return Series(diff)


# transform series into train and test sets for supervised learning
def prepare_data(series, n_test, n_lag, n_seq):
    # extract raw values
    raw_values = series.values
    # transform data to be stationary
    diff_series = difference(raw_values, 1)
    diff_values = diff_series.values
    diff_values = diff_values.reshape(len(diff_values), 1)
    # rescale values to -1, 1
    #scaler = MinMaxScaler(feature_range=(-1, 1))
    #scaled_values = scaler.fit_transform(diff_values)
    #scaled_values = scaled_values.reshape(len(scaled_values), 1)
    # transform into supervised learning problem X, y
    supervised = series_to_supervised(diff_values, n_lag, n_seq)
    supervised_values = supervised.values
    # split into train and test sets
    train, test = supervised_values[0:-n_test], supervised_values[-n_test:]
    return train, test


# fit an LSTM network to training data
def fit_lstm(samples_train):
    # reshape training into [samples, timesteps, features]
    #X, y = train[:, 0:n_lag], train[:, n_lag:]
    #X = X.reshape(X.shape[0], 1, X.shape[1])
    # design network
    i = Input(shape=(samples_train.shape[1], samples_train.shape[2]), dtype='float32')
    enc = GRU(150, return_sequences=True, activation='linear')(i)
    dec = AttentionDecoder(150, samples_train.shape[2])(enc)
    model = Model(inputs=i, outputs=dec)
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    # fit network
    return model


# make one forecast with an LSTM,
def forecast_lstm(model, X):
    # reshape input pattern to [samples, timesteps, features]
    X = X.reshape(1, 1, len(X))
    # make forecast
    forecast = model.predict(X)
    # convert to array
    return [x for x in forecast[0, :]]

# evaluate the persistence model
def make_forecasts(model, test, n_lag):
    forecasts = list()
    for i in range(len(test)):
        X, y = test[i, 0:n_lag], test[i, n_lag:]
        # make forecast
        forecast = forecast_lstm(model, X)
        # store the forecast
        forecasts.append(forecast)
    return forecasts


# invert differenced forecast
def inverse_difference(last_ob, forecast):
    # invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last_ob)
    # propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i - 1])
    return inverted


# inverse data transform on forecasts
def inverse_transform(series, forecasts, scaler, n_test):
    inverted = list()
    for i in range(len(forecasts)):
        # create array from forecast
        forecast = array(forecasts[i])
        forecast = forecast.reshape(1, len(forecast))
        # invert scaling
        inv_scale = scaler.inverse_transform(forecast)
        inv_scale = inv_scale[0, :]
        # invert differencing
        index = len(series) - n_test + i - 1
        last_ob = series.values[index]
        inv_diff = inverse_difference(last_ob, inv_scale)
        # store
        inverted.append(inv_diff)
    return inverted


# evaluate the RMSE for each forecast time step
def evaluate_forecasts(test, forecasts, n_lag, n_seq):
    for i in range(n_seq):
        actual = [row[i] for row in test]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = sqrt(mean_squared_error(actual, predicted))
        print('t+%d RMSE: %f' % ((i + 1), rmse))


# plot the forecasts in the context of the original dataset
def plot_forecasts(series, forecasts, n_test):
    # plot the entire dataset in blue
    pyplot.plot(series.values)
    # plot the forecasts in red
    for i in range(len(forecasts)):
        off_s = len(series) - n_test + i - 1
        off_e = off_s + len(forecasts[i]) + 1
        xaxis = [x for x in range(off_s, off_e)]
        yaxis = [series.values[off_s]] + forecasts[i]
        pyplot.plot(xaxis, yaxis, color='red')
    # show the plot
    pyplot.show()

def sampling(final_train_diff,final_test_diff):

    X_train, y_train = final_train_diff[:, 0:1], final_train_diff[:, 1:]
    X_test, y_test = final_test_diff[:, 0:1], final_test_diff[:, 1:]
    zeros_1 = np.zeros((X_train.shape[0], 1), dtype='int64')
    X_train = np.append(X_train, zeros_1, axis=1)
    X_train = np.append(X_train, zeros_1, axis=1)
    X_train = np.append(X_train, zeros_1, axis=1)
    X_train = np.append(X_train, zeros_1, axis=1)
    zeros_1 = np.zeros((X_test.shape[0], 1), dtype='int64')
    X_test = np.append(X_test, zeros_1, axis=1)
    X_test = np.append(X_test, zeros_1, axis=1)
    X_test = np.append(X_test, zeros_1, axis=1)
    X_test = np.append(X_test, zeros_1, axis=1)

    final_train = X_train.tolist()
    final_test = X_test.tolist()
    train_target = y_train.tolist()
    test_target = y_test.tolist()

    length = 5
    n = len(X_train)
    samples_train = []
    for i in range(0, n, length):
        sample_tr = final_train[i:i + length]
        if len(sample_tr) == length:
            samples_train.append(sample_tr)

    l = len(X_test)
    samples_test = []
    for j in range(0, l, length):
        sample_ts = final_test[j:j + length]
        if len(sample_ts) == length:
            samples_test.append(sample_ts)

    k = len(y_train)
    samples_train_target = []
    for k in range(0, k, length):
        sample_trt = train_target[k:k + length]
        if len(sample_trt) == length:
            samples_train_target.append(sample_trt)

    m = len(y_test)
    samples_test_target = []
    for m in range(0, m, length):
        sample_tst = test_target[m:m + length]
        if len(sample_tst) == length:
            samples_test_target.append(sample_tst)

    samples_train = np.array(samples_train)
    samples_test = np.array(samples_test)
    samples_train_target = np.array(samples_train_target)
    samples_test_target = np.array(samples_test_target)

    return samples_train,samples_test,samples_train_target,samples_test_target

