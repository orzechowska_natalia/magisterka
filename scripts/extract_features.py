from scripts.generate_interim import InterimDataGenerator
import json
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import string
import re
interimdatagenerator = InterimDataGenerator()

base_dir = 'C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/raw'
list_of_files = interimdatagenerator.load_file_list(base_dir)
#samogłoski =['a', 'e', 'y', 'i', 'o', 'ą', 'ę','ó','u']
#polskie_znaki = ['ć','ą','ę','ó','ź','ż']
list_of_dfs = []

for file in list_of_files:
    dir = base_dir + '/' + file
    word = file.replace('.json','')
    with open(dir, encoding='UTF-8') as f:
        data = json.load(f)
    try:
        list_of_dicts = data['response']['docs']
        dataFrame = pd.DataFrame(list_of_dicts)
        wanted_features = dataFrame[['origin','original_type','duration_start']]
    except:
        continue
    wanted_features.insert(0,'word',word)
    list_of_dfs.append(wanted_features)

final_df = pd.concat(list_of_dfs).reset_index(drop=True)
final_keys_groups = []
final_df_groups = final_df.groupby('original_type')
all_grouped_keys = list(final_df_groups.groups.keys())
table = str.maketrans(dict.fromkeys(string.punctuation))
final_df['original_type'] = final_df['original_type'].replace(float('nan'),'nieznany')
for key in all_grouped_keys:
    new_key = key.split('//')
    word_to_add = new_key[0].strip().lower()
    clean_word_to_add = word_to_add.split(' ')
    if len(clean_word_to_add) != 1:
        final_word_to_add = clean_word_to_add[0]
        final_word_to_add = final_word_to_add.translate(table)
    else:
        final_word_to_add = word_to_add
        final_word_to_add = final_word_to_add.translate(table)
    if final_word_to_add not in final_keys_groups:
        final_keys_groups.append(final_word_to_add)

for index, row in final_df.iterrows():
    word_to_find = row.original_type.split('//')
    word_to_add = word_to_find[0].strip().lower()
    clean_word_to_add = word_to_add.split(' ')
    if row.original_type != 'nieznany':
        if row.original_type == 'book':
            row.original_type == 'książka'
        elif row.original_type == 'article':
            row.original_type == 'artykuł'
        elif row.original_type == 'document':
            row.original_type == 'dokument'
            continue
        if len(clean_word_to_add) != 1:
            final_word_to_add = clean_word_to_add[0]
            final_word_to_add = final_word_to_add.translate(table)
        else:
            final_word_to_add = word_to_add
            final_word_to_add = final_word_to_add.translate(table)
        finded = [word for word in final_keys_groups if word == final_word_to_add]
        try:
            final_df.at[index,'original_type'] = finded[0]
        except:
            pass

final_df.origin.replace(['in','fg'],[1,0],inplace=True)
# Get one hot encoding of columns 'vehicleType'
one_hot = pd.get_dummies(final_df['original_type'])
# Drop column as it is now encoded
final_df = final_df.drop('original_type',axis = 1)
# Join the encoded df
final_df = final_df.join(one_hot)
final_df = final_df.to_csv('C:/Users/Mati/PycharmProjects/obrobka_tekstu/data/interim/cechy/dummies.tsv',
                           sep = '\t', index = False)
print('bye')
