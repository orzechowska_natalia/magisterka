from scripts.generate_interim import load_file_list
import pandas as pd

def get_dataframe():

    list_of_files = load_file_list('słowa_wystąpienia')
    list_of_files = [ file.replace('.tsv','') for file in list_of_files]

    # first dict
    spolgloski = ['b', 'c', 'ć', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'w', 'y', 'z', 'ż', 'ź']
    samogłoski = ['a', 'e', 'y', 'u', 'i', 'o', 'ó' ,'ą', 'ę']
    slowo = []
    slowa = []
    for file in list_of_files:
        zakodowane_slowo = ''.join(format(ord(x),'b')for x in file)
        dlugosc_zakodowanego_slowa = len(file)
        if file[0] in samogłoski:
            czy_samogloska = 1
        else:
            czy_samogloska = 0
        slowa.append([zakodowane_slowo,dlugosc_zakodowanego_slowa,czy_samogloska])


    df = pd.DataFrame(slowa, index = list_of_files)
    print('bye')
    return df

def prepare_timeseries():
    plik = pd.read_csv('kontyngensowy.tsv',sep='\t',header=None)
    plik.columns = ['data','ilosc_wystapien']
    begin = plik.iloc[0][0]
    end = plik.iloc[(len(plik)-1)][0]
    date_range = pd.date_range(begin,end,
                  freq='MS').strftime("%Y-%m").tolist()
    df_date_range = pd.DataFrame(date_range)
    df_date_range.columns = ['data']
    plik['data'] = pd.to_datetime(plik['data'], errors='coerce')
    plik['data'] = plik['data'].dt.strftime("%Y-%m")
    result = pd.merge(df_date_range,plik,on ='data',how='left')
    result['ilosc_wystapien'] = result['ilosc_wystapien'].fillna(0)
    result.set_index('data')
    result.plot()
    print('bye')

prepare_timeseries()


# second dict






