import pandas as pd
from scripts.generate_interim import InterimDataGenerator


class GenerateIn(object):

    #klasa do tworzenia pliku in.tsv

    interimdatagenerator = InterimDataGenerator()

    def __init__(self,interimdatagenerator):
        self.interimdatagenerator = interimdatagenerator

    def get_df(self,directory,col_names):

        dataframe = pd.read_csv(directory,sep='\t',names = col_names)
        return dataframe

    def generate_expected(self,input_dir,output_dir):

        final_output_dir = output_dir + '/' + 'in.tsv'
        file_list = self.interimdatagenerator.load_file_list(input_dir)
        #data frame od in.tsv z wszystkimi słowami
        main_df = self.get_df(final_output_dir,['slowo','rok'])
        list_of_results = []

        for file in file_list:
            word = file.replace('.tsv','')
            mydir = input_dir + '/' + file
            #data frame dla jednego słowa (jego wystąpień)
            df = self.get_df(mydir,col_names=['data','ilosc_wystapien'])
            df['rok'] = pd.to_datetime(df['data'], errors='coerce')
            df['rok'] = df['rok'].dt.year
            main_df['rok'] = pd.to_numeric(main_df['rok'],errors='coerce')
            # df dla danego jednego słowa grupujemy po roku w zależności od ilości wystąpień
            df = df.groupby(['rok']).size()
            df = pd.DataFrame({'rok':df.index, 'ilosc_wystapien':df.values})
            # wybieramy tylko te lata które są w pliku in.tsv do nich przypisujemy wartości
            new_main_df = main_df.loc[main_df['slowo'] ==  word]
            # łączymy po "roku"
            result = pd.merge(new_main_df,df,on='rok',how='left')
            # wyniki
            list_of_results.append(result)

        merged = pd.concat(list_of_results)
        # uzupełniamy zero tam gdzie NA
        merged['ilosc_wystapien'] = merged['ilosc_wystapien'].fillna(0)
        my_output_dir = output_dir + '/' + 'expected.tsv'
        # tworzony expected.tsv z 3 kolumnami , nie do końca ok
        merged.to_csv(my_output_dir, sep='\t',index=False,header=None)

#generate_output()




